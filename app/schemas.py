from jsonschema import validate
import json

# Schema Definitions
taskSchema = {
    "definitions": {
        "task": {
            "type": "object",
            "properties": {
                "title": {"type": "string"},
                "due_date": {"type": "string"},
                "assignee": {"type": "string"},
                "description": {"type":"string"},
            },
            "required": ["name", "due_date", "assignee", "description"]
        }
    }
}

wedSchema = {
    "definitions": {
        "task": {
            "type": "object",
            "properties": {
                "title": {"type": "string"},
                "due_date": {"type": "string"},
                "assignee": {"type": "string"},
                "description": {"type":"string"},
            },
            "required": ["name", "due_date", "assignee", "description"],
        },

        "guest": {
            "type": "object",
            "properties": {
                "fname": {"type": "string"},
                "lname": {"type": "string"},
                "plus_one": {"type": "boolean"},
                "count": {"type": "integer"},
            },
            "required": ["fname", "lname", "plus_one", "count"]
        },

        "venue": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "location": {"type": "string"},
                "cost": {"type": "number"},
                "website": {"type": "string"},
                "phone_number": {"type": "string"},
                "contact_person": {"type": "string"},
            },
            "required": ["name", "location"]
        },
    },
    
    "type": "object",
    "properties": {
        "creator": {"type": "string"},
        "name": {"type": "string"},
        "date": {"type": "string"},
        "location": {"type": "string"},
        "planners": {"type":"array", "items": {"type": "string"}}, 
        "tasks": {"type": "array", "items": {"$ref": "#/definitions/task"}},
        "guests": {"type": "array", "items": {"$ref": "#/definitions/guest"}},
        "guest_count": {"type": "integer"},
        "venues": {"type": "array", "items": {"$ref": "#/definitions/venue"}},
    },
    "required": ["creator", "name"],
}


# Sample json to validate schemas
sampleTask = {
    "name": "Book Venue",
    "due_date": "05-24-2019",
    "assignee": "Orion",
    "description": "Book the wedding venue."
}

sampleGuest = {
    "fname": "Mike",
    "lname": "Davis",
    "plus_one": False,
    "count": 1
}

guests = [sampleGuest, sampleGuest]

sampleWedding = {
    "name": "Davis-Kassinger Wedding",
    "date": "May 24, 2020",
    "tasks": [sampleTask],
    "guests": guests,
    "guest_count": len(guests),
}

print(validate(instance=sampleWedding, schema=wedSchema))
print(json.dumps(sampleWedding, indent=2))

