from app import app
from flask import request, redirect

from app.models import DB

import json

db = DB()

true = True
false = False

# Test data
user_data = {'username': 'jper99', 'name': 'jake peralta', 'purpose': 'function testing'}
user_credentials = {'username': 'jper99', 'password':'hltismydad'}
super_wedding = {'date': 'October 31, 2019', 'planners': ['rut'], 'location': 'Halloween Town'}
task_data = {'id':'50', 'title':'Book venue', 'desc':'Find and book a venue'}

test_update = {
 "original": {
    "fname": "Terry",
    "lname": "Crews",
    "plus_one": true,
    "count": 2
 },

"updated": {
    "fname": "Terry",
    "lname": "Crews",
    "plus_one": false,
    "count": 1
},
}
@app.route('/')
def index():
    db.update_guest("_awesome-wedding", test_update)
    return "Index! :)"

# 
# Manage users
@app.route('/wedplan/api/v0.1/users', methods=['POST'])
def create_user():
    req = request.get_json()

    try:
        ret = db.add_user(req)
        return json.dumps(ret), 201
    except:
        return json.dumps(req), 409

@app.route('/wedplan/api/v0.1/users/auth', methods=['POST'])
def authenticate_user():
    req = request.get_json()

    try:
        ret = db.auth_user(req)
        
        return json.dumps(ret), 200
    except:
        return json.dumps(req), 409
# @app.route('/wedplan/api/v0.1/users/<user_id>', methods=['PUT'])

# 
# Manage weddings
@app.route('/wedplan/api/v0.1/weddings', methods=['POST'])
def create_wedding():
    req = request.get_json()

    try:
        ret = db.add_wedding(req)
        return json.dumps(ret), 201
    except:
        return json.dumps(req), 500

@app.route('/wedplan/api/v0.1/weddings/<wedding_id>', methods=['GET'])
def retrieve_wedding(wedding_id):
    try:
        return json.dumps(db.retrieve_wedding_doc(wedding_id)), 200
    except:
        return json.dumps(wedding_id), 404
    

# # Manage planning tasks
@app.route('/wedplan/api/v0.1/tasks/<doc_key>', methods=['GET'])
def retrieve_task_list(doc_key):
    return json.dumps(db.retrieve_task_list(doc_key)), 200

@app.route('/wedplan/api/v0.1/venues/<doc_key>', methods=['GET'])
def retrieve_venue_list(doc_key):
        ret = db.retrieve_venue_list(doc_key)
        return json.dumps(ret), 200

@app.route('/wedplan/api/v0.1/tasks', methods=['POST'])
def create_task():
    req = request.get_json()

    try:
        ret = db.create_task(req)
        return json.dumps(ret), 201
    except:
        return json.dumps(req), 400

@app.route('/wedplan/api/v0.1/venues', methods=['POST'])
def create_venue():
    req = request.get_json()

    try:
        ret = db.create_venue(req)
        return json.dumps(ret), 201
    except:
        return json.dumps(req), 400

    
@app.route('/wedplan/api/v0.1/tasks/<doc_key>', methods=['PUT'])
def update_task_info(doc_key):
    req = request.get_json()

    try:
        ret = db.update_task(doc_key, req)
        return json.dumps(ret), 201
    except:
        return json.dumps(req), 400

@app.route('/wedplan/api/v0.1/venues/<doc_key>', methods=['PUT'])
def update_venue_info(doc_key):
    req = request.get_json()

    try:
        ret = db.update_venue(doc_key, req)
        return json.dumps(ret), 201
    except:
        return json.dumps(req), 400

    
@app.route('/wedplan/api/v0.1/tasks/<doc_key>/<task_pos>', methods=['DELETE'])
def delete_task(doc_key, task_pos):
    try:
        db.delete_task(doc_key, task_pos)
        return json.dumps({"Response": "Good"}), 200
    except:
        return json.dumps({"Response": "Bad"}), 400
    
@app.route('/wedplan/api/v0.1/venues/<doc_key>/<venue_pos>', methods=['DELETE'])
def delete_venue(doc_key, venue_pos):
    try:
        db.delete_venue(doc_key, venue_pos)
        return json.dumps({"Response": "Good"}), 200
    except:
        return json.dumps({"Response": "Bad"}), 400

    
#
# Manage guest list
# 1. Get guest list
# 2. Add guest
# 3. Update guest
# 4. Delete guest
@app.route('/wedplan/api/v0.1/guests/<doc_id>', methods=['GET'])
def retrieve_guest_list(doc_id):
    try:
        return json.dumps(db.retrieve_guest_list(doc_id))
    except:
        return json.dumps(doc_id), 404

@app.route('/wedplan/api/v0.1/guests', methods=['POST'])
def create_guest():
    req = request.get_json()

    try:
        ret = db.create_guest(req)
        return json.dumps(ret), 201
    except:
        return json.dumps(req), 400
    
@app.route('/wedplan/api/v0.1/guests/<doc_key>', methods=['PUT'])
def update_guest_info(doc_key):
    req = request.get_json()

    try:
        ret = db.update_guest(doc_key, req)
        return json.dumps(ret), 200
    except:
        return json.dumps(req), 400
    
@app.route('/wedplan/api/v0.1/guests/<doc_key>/<guest_pos>', methods=['DELETE'])
def delete_guest(doc_key, guest_pos):
    try:
        db.delete_guest(doc_key, guest_pos)
        return json.dumps({"response": "Good"}), 200
    except:
        return json.dumps({"response": "Bad"}), 400
    
# # Manage venue search options
# @app.route('/wedplan/api/v0.1/venues', methods=['GET'])
# @app.route('/wedplan/api/v0.1/venues/<venue_id>', methods=['GET'])
# @app.route('/wedplan/api/v0.1/venues', methods=['POST'])
# @app.route('/wedplan/api/v0.1/venues/<venue_id>', methods=['PUT'])
# @app.route('/wedplan/api/v0.1/venues/<venue_id>', methods=['DELETE'])
