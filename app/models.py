from pyArango.connection import *

true = True
false = False

wedSchema = {
    "definitions": {
        "task": {
            "type": "object",
            "properties": {
                "title": {"type": "string"},
                "due_date": {"type": "string"},
                "assignee": {"type": "string"},
                "description": {"type":"string"},
            },
            "required": ["name", "due_date", "assignee", "description"],
        },

        "guest": {
            "type": "object",
            "properties": {
                "fname": {"type": "string"},
                "lname": {"type": "string"},
                "plus_one": {"type": "boolean"},
                "count": {"type": "integer"},
            },
            "required": ["fname", "lname", "plus_one", "count"]
        },

        "venue": {
            "type": "object",
            "properties": {
                "name": {"type": "string"},
                "location": {"type": "string"},
                "cost": {"type": "number"},
                "website": {"type": "string"},
                "phone_number": {"type": "string"},
                "contact_person": {"type": "string"},
            },
            "required": ["name", "location"]
        },
    },
    
    "type": "object",
    "properties": {
        "creator": {"type": "string"},
        "name": {"type": "string"},
        "date": {"type": "string"},
        "location": {"type": "string"},
        "planners": {"type":"array", "items": {"type": "string"}}, 
        "tasks": {"type": "array", "items": {"$ref": "#/definitions/task"}},
        "guests": {"type": "array", "items": {"$ref": "#/definitions/guest"}},
        "guest_count": {"type": "integer"},
        "venues": {"type": "array", "items": {"$ref": "#/definitions/venue"}},
    },
    "required": ["creator", "name"],
}

class DB():
    def __init__(self):
        # Create the database if it does not exist, or create
        # connection if it does
        try:
            conn = Connection().createDatabase(name="wedplan")
        except:
            conn = Connection()

        self.db = conn["wedplan"]

        # Create the necessary collections, or grab them if they exist
        # FIXME: Could fail if users exists but not weddings
        try:
            user_collection = self.db.createCollection(name="users")
            wedding_collection = self.db.createCollection(name="weddings")
        except:
            user_collection = self.db["users"]
            wedding_collection = self.db["weddings"]
            
        self.user_col = self.db["users"]
        self.wed_col = self.db["weddings"]

    def add_user(self, prop):
        doc = self.user_col.createDocument()
        
        for k,v in prop.items():
            doc[k] = v

        doc._key = '_' + prop["username"]

        doc.save()

        # An error will be received if the entered username is not
        # unique due to it being part of the document key
        return doc.getStore()

    def auth_user(self, creds):
        user_doc = self.user_col.fetchDocument('_'+creds["username"])

        if creds["password"] == user_doc["password"]:
            return user_doc.getStore()
        else:
            return None

    def add_wedding(self, prop):
        doc = self.wed_col.createDocument()

        for k,v in prop.items():
            doc[k] = v

        doc["planners"] = []
        doc["tasks"] = []
        doc["guests"] = []
        doc["guest_count"] = 0
        doc["venues"] = []

        doc._key = '_'+prop["name"]

        # validate(instance=doc, schema=wedSchema)

        doc.save()

        user_doc = self.user_col.fetchDocument(prop["creator"])
        user_doc["wedding_doc"] = doc._key
        user_doc.save()

        return doc.getStore()

    def retrieve_wedding_doc(self, wedding_doc_key):
        return self.wed_col.fetchDocument(wedding_doc_key).getStore()

    def retrieve_guest_list(self, wedding_doc_key):
        doc = self.retrieve_wedding_doc(wedding_doc_key)
        guests = doc["guests"]
        guest_count = doc["guest_count"]
        return {"guests": guests, "guest_count": guest_count}

    def retrieve_task_list(self, wedding_doc_key):
        doc = self.retrieve_wedding_doc(wedding_doc_key)

        tasks = doc["tasks"]

        return {"tasks": tasks}

    def retrieve_venue_list(self, wedding_doc_key):
        doc = self.retrieve_wedding_doc(wedding_doc_key)

        venues = doc["venues"]

        return {"venues": venues}

    def create_guest(self, prop):
        doc = self.wed_col.fetchDocument(prop["doc_key"])
        del prop["doc_key"]

        guest_list = doc["guests"]

        guest = {}
        for k,v in prop.items():
            guest[k] = v

        guest_list.append(guest)

        if guest["plus_one"] == True:
            doc["guest_count"] += 2
        else:
            doc["guest_count"] += 1

        doc.save()

        return doc.getStore()

    def create_task(self, prop):
        doc = self.wed_col.fetchDocument(prop["doc_key"])
        del prop["doc_key"]

        task_list = doc["tasks"]

        task = {}
        for k,v in prop.items():
            task[k] = v

        task_list.append(task)

        doc.save()

        return doc.getStore()

    def create_venue(self, prop):
        doc = self.wed_col.fetchDocument(prop["doc_key"])
        del prop["doc_key"]

        venue_list = doc["venues"]

        venue = {}
        for k,v in prop.items():
            venue[k] = v

        venue_list.append(venue)

        doc.save()

        return doc.getStore()

    def update_guest(self, doc_key, prop):
        doc = self.wed_col.fetchDocument(doc_key)

        guest_list = doc["guests"]
        guest = prop["original"]
        
        guest_found = guest in guest_list

        if guest_found:
            pos = guest_list.index(guest)
            del guest_list[pos]

            updated_guest = prop["updated"]

            guest_list.insert(pos, updated_guest)

            doc["guest_count"] -= guest["count"]
            doc["guest_count"] += updated_guest["count"]

            doc.save()

            return updated_guest
        else:
            raise LookupError

    def update_task(self, doc_key, prop):
        doc = self.wed_col.fetchDocument(doc_key)

        task_list = doc["tasks"]
        task = prop["original"]

        task_found = task in task_list

        if task_found:
            pos = task_list.index(task)
            del task_list[pos]

            updated_task = prop["updated"]

            task_list.insert(pos, updated_task)

            doc.save()

            return updated_task
        else:
            return LookupError

    def update_venue(self, doc_key, prop):
        doc = self.wed_col.fetchDocument(doc_key)

        venue_list = doc["venues"]
        venue = prop["original"]

        venue_found = venue in venue_list

        if venue_found:
            pos = venue_list.index(venue)
            del venue_list[pos]

            updated_venue = prop["updated"]

            venue_list.insert(pos, updated_venue)

            doc.save()

            return updated_venue
        else:
            return LookupError

    def delete_guest(self, doc_key, guest_pos):
        doc = self.wed_col.fetchDocument(doc_key)

        guest_list = doc["guests"]
        guest = guest_list[int(guest_pos)]
        
        del guest_list[int(guest_pos)]

        doc["guest_count"] -= guest["count"]
        
        doc.save()

    def delete_task(self, doc_key, task_pos):
        doc = self.wed_col.fetchDocument(doc_key)

        task_list = doc["tasks"]
        task = task_list[int(task_pos)]

        del task_list[int(task_pos)]

        doc.save()

    def delete_venue(self, doc_key, venue_pos):
        doc = self.wed_col.fetchDocument(doc_key)

        venue_list = doc["venues"]
        venue = venue_list[int(venue_pos)]

        del venue_list[int(venue_pos)]

        doc.save()
                

