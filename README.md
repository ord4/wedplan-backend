# Wedding Planner Backend
This project contains the backend logic for my Computer Science Senior
Seminar project. It is implemented using Python Flask, a python
microframework for creating web applications. Within this project is
definition of different REST API endpoints which will be called from
the phone application. The source code for that application can be
found [here]().

## Running
To run the application start up a python virtual environment and
execute a `pip install -r requirements.txt` command to get all the
application's dependencies. Then, in the project's top directory
ensure your `FLASK_APP` environment variable is set to `run.py`. Once
these two steps have been completed you can then run the application
with a simple `flask run`.
